<%-- 
    Document   : index
    Created on : 17-Jun-2016, 16:53:18
    Author     : Selvyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper" class="deutschebank.thebeans.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script type="text/javascript" src="jquery-3.0.0.js"></script>
        <script type="text/javascript" src="userdetails_validator.js"></script>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <style type="text/css">
                .dock{
                        z-index: 100; 
                        border: 2px; 
                        box-shadow: 0 0 30px black; 
                        padding: 100px 15px 100px 15px; 
                        margin-top: 150px; 
                        width: 420px; 
                        margin-right: 10%;
                        background: white;
                        opacity: 0.9;
                        border-radius: 0.2;
                }
        </style>
    </head>
    
    <body background="bg.jpg">
        <%
            String  dbStatus = "DB NOT CONNECTED";

            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();
            
            if( connectionStatus )
            {
                dbStatus = "You have successfully connected the DB server";
            }
        %>
        <h2><%= dbStatus %></h2>
        <%
            if( connectionStatus )
            {
        %>
        <script> console.log(<%= globalHelper.getData("counterparty") %>); </script>
        <section>
		<div class="container pull-right dock">
			<form class="form-horizontal" action = "validate.jsp" method = "GET">
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="f_userid" name="id" placeholder="Email">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
			    <div class="col-sm-10">
			      <input type="password" class="form-control" id="f_pwd" name="password" placeholder="Password">
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <div class="checkbox">
			        <label>
			          <input type="checkbox"> Remember me
			        </label>
			      </div>
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-primary" onclick="validateUserId()">Sign in</button>
			    </div>
			  </div>
			</form>
		</div>
	</section>
	<footer></footer>   
        <%
            }
        %>
    </body>
</html>
