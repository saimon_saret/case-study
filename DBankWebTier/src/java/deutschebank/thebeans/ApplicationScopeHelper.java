package deutschebank.thebeans;

import deutschebank.MainUnit;
import deutschebank.dbutils.DBConnector;
import deutschebank.dbutils.instrument.Instrument;
import deutschebank.dbutils.instrument.InstrumentHandler;
import deutschebank.dbutils.PropertyLoader;
import deutschebank.dbutils.combined.Combined;
import deutschebank.dbutils.combined.CombinedHandler;
import deutschebank.dbutils.counterparty.Counterparty;
import deutschebank.dbutils.counterparty.CounterpartyHandler;
import deutschebank.dbutils.user.User;
import deutschebank.dbutils.user.UserHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ApplicationScopeHelper
{
    private String  itsInfo = "NOT SET";
    private DBConnector itsConnector = null;

    public String getInfo()
    {
        return itsInfo;
    }

    public void setInfo(String itsInfo)
    {
        this.itsInfo = itsInfo;
    }
    
    public  boolean    bootstrapDBConnection()
    {
        boolean result = false;
        try
        {
            itsConnector = DBConnector.getConnector();

            PropertyLoader pLoader = PropertyLoader.getLoader();

            Properties pp;
            pp = pLoader.getPropValues( "dbConnector.properties" );
            
            result = itsConnector.connect( pp );
        } 
        catch (IOException ex)
        {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public User userLogin( String userId, String userPwd )
    {
        User theUser = null;
        UserHandler theUserHandler = UserHandler.getLoader();
            
        theUser = theUserHandler.loadFromDB(itsConnector.getConnection(), userId, userPwd );

        if( theUser != null )
                MainUnit.log( "User " + userId + " has logged into system");

        return theUser;
    }
    
    public String getData(String tableName) {
        String res = "";
        switch(tableName) {
            case "combined":
                CombinedHandler handler = CombinedHandler.getLoader();
                ArrayList<Combined> deals = handler.loadCombinedData(itsConnector.getConnection());
                return handler.toJSON(deals);
            case "counterparty":
                CounterpartyHandler counterpartyHandler = CounterpartyHandler.getLoader();
                ArrayList<Counterparty> counterparties = counterpartyHandler.loadCounterpartyData(itsConnector.getConnection());
                return counterpartyHandler.toJSON(counterparties); 
            case "instrument":
                InstrumentHandler instrumentHandler = InstrumentHandler.getLoader();
                ArrayList<Instrument> instrumentDeals = instrumentHandler.loadInstrumentData(itsConnector.getConnection());
                return instrumentHandler.toJSON(instrumentDeals); 
        }
        return res;
    }
    
    public String getData(String tableName, int lowerKey, int upperKey) {
        String res = "";
        switch(tableName) {
            case "combined":
                CombinedHandler combinedHandler = CombinedHandler.getLoader();
                ArrayList<Combined> combinedDeals = combinedHandler.loadCombinedData(itsConnector.getConnection(), lowerKey, upperKey);
                return combinedHandler.toJSON(combinedDeals);
            case "counterparty":
                CounterpartyHandler counterpartyHandler = CounterpartyHandler.getLoader();
                ArrayList<Counterparty> counterparties = counterpartyHandler.loadCounterpartyData(itsConnector.getConnection(), lowerKey, upperKey);
                return counterpartyHandler.toJSON(counterparties); 
            case "instrument":
                InstrumentHandler instrumentHandler = InstrumentHandler.getLoader();
                ArrayList<Instrument> instruments = instrumentHandler.loadInstrumentData(itsConnector.getConnection(), lowerKey, upperKey);
                return instrumentHandler.toJSON(instruments); 
        }
        return res;
    }
}
