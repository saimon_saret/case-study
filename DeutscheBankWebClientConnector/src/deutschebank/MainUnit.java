/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank;

import deutschebank.dbutils.DBConnector;
import deutschebank.dbutils.instrument.Instrument;
import deutschebank.dbutils.instrument.InstrumentHandler;
import deutschebank.dbutils.PropertyLoader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.dbutils.combined.Combined;
import deutschebank.dbutils.combined.CombinedHandler;
import deutschebank.dbutils.user.User;
import deutschebank.dbutils.user.UserHandler;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{
   private static final Logger LOGGER = Logger.getLogger(LoggerExample.class.getName());
   public  static  boolean debugFlag = true;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            DBConnector connector = DBConnector.getConnector();
            
            PropertyLoader pLoader = PropertyLoader.getLoader();
            
            Properties pp = pLoader.getPropValues( "dbConnector.properties" );
            
            connector.connect( pp );
            
            InstrumentHandler theInstrumentHandler = InstrumentHandler.getLoader();

            ArrayList<Instrument> theInstruments = theInstrumentHandler.loadInstrumentData(connector.getConnection());
            
            Instrument[] insArray = new Instrument[theInstruments.size()];
            theInstruments.toArray(insArray);
            theInstruments.forEach( (instrument)->
                {
                    System.out.println( instrument.getInstrumentID() + "//" + instrument.getInstrumentName() );
                }
            );
            
             
            CombinedHandler theCombinedHandler = CombinedHandler.getLoader();
            ArrayList<Combined> theDeals = theCombinedHandler.loadCombinedData(connector.getConnection(), 20001, 20010);
            
            Combined[] dealArray = new Combined[theDeals.size()];
            theDeals.toArray(dealArray);
            
            // Now convert the Instrument instane into a JSON object
            ObjectMapper mapper = new ObjectMapper();

            // Convert an array of objects to JSON string and save into a file directly
            mapper.writeValue(new File("instrument_array.json"), insArray);
            
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mapper.setDateFormat(df);
            mapper.writeValue(new File("deal_array.json"), dealArray);

            // Convert object to JSON string
            String jsonInString = mapper.writeValueAsString(theInstruments);
            System.out.println(jsonInString);

            // Convert object to JSON string and pretty print
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theInstruments);
            System.out.println(jsonInString);

            //========================================================================
            // Working with user table
            //========================================================================
            UserHandler theUserHandler = UserHandler.getLoader();
            User theUser = theUserHandler.loadFromDB(connector.getConnection(), "selvyn", "gradprog2016");
            
            if( theUser != null )
            {
                System.out.println( theUser.getUserID()+ "//" + theUser.getUserPwd());
            }

            // Convert object to JSON string and save into a file directly
            mapper.writeValue(new File("user.json"), theUser);

        } 
        catch (IOException ex)
        {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public  static  void    log( String msg )
    {
        //if( debugFlag )
        {
            LOGGER.info( msg );
            System.out.println( msg );
        }
    }
    
}
