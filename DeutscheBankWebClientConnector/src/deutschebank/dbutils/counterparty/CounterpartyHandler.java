package deutschebank.dbutils.counterparty;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CounterpartyHandler
{
    static  private CounterpartyHandler itsSelf = null;
    
    private final String instrumentTemplate = "select * from counterparty";
    private final ObjectMapper mapper;
    
    private CounterpartyHandler() {
        mapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        mapper.setDateFormat(df);
    }
    
    static  public CounterpartyHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new CounterpartyHandler();
        return itsSelf;
    }

    public  ArrayList<Counterparty> loadCounterpartyData(Connection theConnection, int lowerKey, int upperKey)
    {
        ArrayList<Counterparty> result = new ArrayList();
        try
        {
            String sbQuery = instrumentTemplate + " where counterparty_id>=? and counterparty_id<=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setInt(1, lowerKey);
            stmt.setInt(2, upperKey);
            ResultSet rs = stmt.executeQuery();
            result = getData(rs);
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public  ArrayList<Counterparty> loadCounterpartyData(Connection theConnection)
    {
        ArrayList<Counterparty> result = new ArrayList();
        try
        {
            String sbQuery = "select * from counterparty";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            result = getData(rs);
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    public  String  toJSON( ArrayList<Counterparty> theCounterpartys )
    {
        String result = "";
        Counterparty[] insArray = new Counterparty[theCounterpartys.size()];
        theCounterpartys.toArray(insArray);
        try
        {
            result = mapper.writeValueAsString(insArray);
        } 
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    private ArrayList<Counterparty> getData(ResultSet rs) throws SQLException {
        CounterpartyIterator iter = new CounterpartyIterator(rs);
        ArrayList<Counterparty> result = new ArrayList();
        while(iter.next()) {
            result.add(iter.buildCounterparty());
        }
        return result;
    }
}
