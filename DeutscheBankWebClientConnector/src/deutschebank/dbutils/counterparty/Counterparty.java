package deutschebank.dbutils.counterparty;

import java.util.Date;

public class Counterparty
{
    private int     itsCounterpartyID;
    private String  itsCounterpartyName;
    private char    itsCounterpartyStatus;
    private Date    itsCounterpartyDateRegistered;
    
    public  Counterparty(int id, String name, char status, Date date)
    {
        itsCounterpartyID = id;
        itsCounterpartyName = name;
        itsCounterpartyStatus = status;
        itsCounterpartyDateRegistered = date;
    }

    public int getItsCounterpartyID() {
        return itsCounterpartyID;
    }

    public void setItsCounterpartyID(int itsCounterpartyID) {
        this.itsCounterpartyID = itsCounterpartyID;
    }

    public String getItsCounterpartyName() {
        return itsCounterpartyName;
    }

    public void setItsCounterpartyName(String itsCounterpartyName) {
        this.itsCounterpartyName = itsCounterpartyName;
    }

    public char getItsCounterpartyStatus() {
        return itsCounterpartyStatus;
    }

    public void setItsCounterpartyStatus(char itsCounterpartyStatus) {
        this.itsCounterpartyStatus = itsCounterpartyStatus;
    }

    public Date getItsCounterpartyDateRegistered() {
        return itsCounterpartyDateRegistered;
    }

    public void setItsCounterpartyDateRegistered(Date itsCounterpartyDateRegistered) {
        this.itsCounterpartyDateRegistered = itsCounterpartyDateRegistered;
    }
}
