package deutschebank.dbutils.counterparty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class CounterpartyIterator
{
   ResultSet rowIterator;

   CounterpartyIterator( ResultSet rs )
   {
       rowIterator = rs;
   }
   
   public boolean  first() throws SQLException
   {
      return rowIterator.first();
   }
   
   public boolean last() throws SQLException
   {
      return rowIterator.last();
   }
   public boolean next() throws SQLException
   {
      return rowIterator.next();
   }
   
   public boolean prior() throws SQLException
   {
      return rowIterator.previous();
   }

   public   String  getCounterpartyName() throws SQLException
   {
      return rowIterator.getString("counterparty_name");
   }

   public   int  getCounterpartyID() throws SQLException
   {
      return rowIterator.getInt("counterparty_id");
   }
   
   public  char  getCounterpartyStatus() throws SQLException
   {
      return rowIterator.getString("counterparty_status").charAt(0);
   }
   
   public Date getCounterpartyTime() throws SQLException
   {
      return new Date(rowIterator.getTimestamp("counterparty_date_registered").getTime());
   }

   Counterparty   buildCounterparty() throws SQLException
   {
       Counterparty result = new Counterparty( getCounterpartyID(), getCounterpartyName(), 
               getCounterpartyStatus(), getCounterpartyTime());
       return result;
   }
}
