package deutschebank.dbutils.instrument;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InstrumentHandler
{
    static  private InstrumentHandler itsSelf = null;
    
    private final String instrumentTemplate = "select * from instrument";
    private final ObjectMapper mapper;
    
    private InstrumentHandler() {
        mapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        mapper.setDateFormat(df);
    }
    
    static  public  InstrumentHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new InstrumentHandler();
        return itsSelf;
    }

    public  ArrayList<Instrument> loadInstrumentData(Connection theConnection, int lowerKey, int upperKey)
    {
        ArrayList<Instrument> result = new ArrayList();
        try
        {
            String sbQuery = instrumentTemplate + " where instrument_id>=? and instrument_id<=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setInt(1, lowerKey);
            stmt.setInt(2, upperKey);
            ResultSet rs = stmt.executeQuery();
            result = getData(rs);
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public  ArrayList<Instrument> loadInstrumentData(Connection theConnection)
    {
        ArrayList<Instrument> result = new ArrayList();
        try
        {
            String sbQuery = "select * from instrument";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            result = getData(rs);
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    public  String  toJSON( ArrayList<Instrument> theInstruments )
    {
        String result = "";
        Instrument[] insArray = new Instrument[theInstruments.size()];
        theInstruments.toArray(insArray);
        try
        {
            result = mapper.writeValueAsString(insArray);
        } 
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    private ArrayList<Instrument> getData(ResultSet rs) throws SQLException {
        InstrumentIterator iter = new InstrumentIterator(rs);
        ArrayList<Instrument> result = new ArrayList();
        while(iter.next()) {
            result.add(iter.buildInstrument());
        }
        return result;
    }
}
