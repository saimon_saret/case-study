package deutschebank.dbutils.combined;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CombinedHandler
{
    static  private CombinedHandler itsSelf = null;
    
    private final String combinedTemplate = "select deal_id, deal_time, counterparty_name, instrument_name, deal_type, deal_amount, deal_quantity from deal \n" +
                                           "left join counterparty on (deal_counterparty_id=counterparty_id)\n" +
                                           "left join instrument on (deal_instrument_id=instrument_id)";
    private final ObjectMapper mapper;
    
    private CombinedHandler() {
        mapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        mapper.setDateFormat(df);
    }
    
    static  public  CombinedHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new CombinedHandler();
        return itsSelf;
    }
    
    public  ArrayList<Combined> loadCombinedData(Connection theConnection)
    {
        ArrayList<Combined> result = new ArrayList();
        try
        {
            String sbQuery = combinedTemplate;
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();
            result = getData(rs);
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(CombinedHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    public  ArrayList<Combined> loadCombinedData(Connection theConnection, int lowerKey, int upperKey)
    {
        ArrayList<Combined> result = new ArrayList();
        try
        {
            String sbQuery = combinedTemplate + " where deal_id>=? and deal_id<=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setInt(1, lowerKey);
            stmt.setInt(2, upperKey);
            ResultSet rs = stmt.executeQuery();
            result = getData(rs);
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(CombinedHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    public String toJSON( ArrayList<Combined> theDeals )
    {
        String result = "";
        Combined[] insArray = new Combined[theDeals.size()];
        theDeals.toArray(insArray);
        try
        {
            result = mapper.writeValueAsString(insArray);
        } 
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(CombinedHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    private ArrayList<Combined> getData(ResultSet rs) throws SQLException {
        CombinedIterator iter = new CombinedIterator(rs);
        ArrayList<Combined> result = new ArrayList();
        while(iter.next()) {
            result.add(iter.buildDeal());
        }
        return result;
    }
}
