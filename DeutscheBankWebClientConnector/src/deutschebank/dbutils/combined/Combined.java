package deutschebank.dbutils.combined;

import java.util.Date;

public class Combined
{
    private int    dealID;
    private Date   dealTime;
    private String counterpartyName;
    private String instrumentName;
    private char   dealType;
    private float  dealAmount;
    private int    dealQuantity;

    public int getDealID() {
        return dealID;
    }

    public void setDealID(int dealID) {
        this.dealID = dealID;
    }

    public Date getDealTime() {
        return dealTime;
    }

    public void setDealTime(Date dealTime) {
        this.dealTime = dealTime;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public char getDealType() {
        return dealType;
    }

    public void setDealType(char dealType) {
        this.dealType = dealType;
    }

    public float getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(float dealAmount) {
        this.dealAmount = dealAmount;
    }

    public int getDealQuantity() {
        return dealQuantity;
    }

    public void setDealQuantity(int dealQuantity) {
        this.dealQuantity = dealQuantity;
    }
}
