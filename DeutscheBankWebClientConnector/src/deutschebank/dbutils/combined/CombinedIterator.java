package deutschebank.dbutils.combined;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class CombinedIterator
{
   ResultSet rowIterator;

   public CombinedIterator( ResultSet rs )
   {
       rowIterator = rs;
   }
   
   public boolean  first() throws SQLException
   {
      return rowIterator.first();
   }
   
   public boolean last() throws SQLException
   {
      return rowIterator.last();
   }
   public boolean next() throws SQLException
   {
      return rowIterator.next();
   }
   
   public boolean prior() throws SQLException
   {
      return rowIterator.previous();
   }
   
   public   int  getDealID() throws SQLException
   {
      return rowIterator.getInt("deal_id");
   }
   
   public Date getDealTime() throws SQLException
   {
      return new Date(rowIterator.getTimestamp("deal_time").getTime());
   }

   public   String  getCounterpartyName() throws SQLException
   {
      return rowIterator.getString("counterparty_name");
   }
   
   public   String  getInstrumentName() throws SQLException
   {
      return rowIterator.getString("instrument_name");
   }
   
   public  char  getDealType() throws SQLException
   {
      return rowIterator.getString("deal_type").charAt(0);
   }
   
   public  float  getDealAmount() throws SQLException
   {
      return rowIterator.getFloat("deal_amount");
   }
   
   public  int  getDealQuantity() throws SQLException
   {
      return rowIterator.getInt("deal_quantity");
   }

   public Combined   buildDeal() throws SQLException
   {
       Combined result = new Combined();
       result.setDealID(getDealID());
       result.setDealTime(getDealTime());
       result.setCounterpartyName(getCounterpartyName());
       result.setInstrumentName(getInstrumentName());
       result.setDealType(getDealType());
       result.setDealAmount(getDealAmount());
       result.setDealQuantity(getDealQuantity());
       
       return result;
   }
}
